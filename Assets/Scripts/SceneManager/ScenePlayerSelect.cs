﻿using UnityEngine;
using System.Collections;

public class ScenePlayerSelect : MonoBehaviour { 

	public bool isPlay = false;

	private Animation mAnimation;

	public GameObject ButtonPlayer1;
	public GameObject ButtonPlayer2;
	public GameObject SceneManger;
	void Update()
	{
		if(isPlay)
		{
			if(mAnimation.isPlaying == false)
			{
				this.gameObject.SetActive(false);
				SceneManger.GetComponent<SceneManager>().StepAdd();
			}
		}
	}

	public void setButton1PlayerAnimation()
	{
		mAnimation = ButtonPlayer1.GetComponent<Animation>();
	}

	public void setButton2PlayerAnimation()
	{
		mAnimation = ButtonPlayer2.GetComponent<Animation>();
	}

	public void setOnPlay()
	{
		isPlay = true;
	}
}
