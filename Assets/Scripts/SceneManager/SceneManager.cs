﻿using UnityEngine;

public class SceneManager : MonoBehaviour
{
	// Scene
	public GameObject logo;
	public GameObject title;
	public GameObject playerSelect;
	public GameObject stage1;
	public GameObject stage2;
	public GameObject result;
	public GameObject gameOver;
	public GameObject gameClear;

	// Fade
	public GameObject fade;
	public GameObject fadeLogo;

	// Players 
	public GameObject m_marika;
	public GameObject m_jack;

	static public bool gameClearFlag;

	public int m_step;
	private float m_time;
	public bool m_startTitle;

	//	1がマリカ
	//	２がジャック
	public static int m_create_char;
	
	enum kSceneState{
		Scene_Logo = 0,
		Scene_Title,
		Scene_PlayerSelect,
		Scene_Stage1,
		Scene_Stage2,
		Scene_Result,
		Scene_GameOver,
		Scene_GameClear,
		Scene_Total
	};

	void Start ()
	{
		fade.GetComponent<FadeTexture>().startFadeOut();
		fadeLogo.GetComponent<ImageLogo>().startFadeOut();
		title.SetActive(false);
		playerSelect.SetActive(false);
		stage1.SetActive(false);
		stage2.SetActive(false);
		gameOver.SetActive(false);
		gameClear.SetActive(false);
		result.SetActive(false);

		gameClearFlag = false;
		m_startTitle = false;
		m_time = 0;

		m_create_char = 0;
	}
	
	void Update ()
	{
		SceneState();

		exitApplication();
	}

	private void exitApplication()
	{
		if (Input.GetKey ("escape")) {
			Application.Quit();
		}
	}

	private void SceneState()
	{	
		switch(m_step)
		{
		case (int)kSceneState.Scene_Logo:
			Fade();

			if(Input.GetKeyDown (KeyCode.X))
			{
				StepAdd();
				logo.SetActive (false);
				title.SetActive(true);
			}
			break;

		case (int)kSceneState.Scene_Title:

			if(m_time < 1.0f)m_time += Time.deltaTime * 0.25f;

			if(m_time >= 1.0f)
			{
				logo.SetActive (false);
				title.SetActive(true);

				title.GetComponent<StageBgm>().playBackGroundMusic();

				if(Input.GetKeyDown (KeyCode.X) || m_startTitle)
				{
					fade.GetComponent<FadeTexture>().setFadeAlphaZero();
					title.GetComponent<StageBgm>().stopBackGroundMusic();
					title.SetActive (false);
					playerSelect.SetActive(true);
					m_startTitle = false;
					m_time = 0.0f;
					StepAdd();
				}
			}

			break;
		case (int)kSceneState.Scene_PlayerSelect:
			break;
		case (int)kSceneState.Scene_Stage1:

			// This is only run once
			if(stage1.activeSelf == false )
			{
				fade.GetComponent<FadeTexture>().startFadeIn();

				stage1.GetComponent<StageEmitter>().ReswapnWave();

				CreatePlayer();

				stage1.SetActive(true);
			}

			stage1.GetComponent<StageBgm>().playBackGroundMusic();

			if(Emitter.stageClear)
			{
				DestroyPlayer();

				stage1.GetComponent<StageEmitter>().DestroyWave();

				stage1.GetComponent<StageBgm>().stopBackGroundMusic();

				stage1.SetActive(false);
				
				result.SetActive(true);

				fade.GetComponent<FadeTexture>().startFadeOut();

				m_step = (int)kSceneState.Scene_Result;
			}

			break;
		case (int)kSceneState.Scene_Stage2:

			// This is only run once
			if(stage2.activeSelf == false )
			{
				fade.GetComponent<FadeTexture>().startFadeIn();

				CreatePlayer();

				stage2.SetActive(true);
			}

			stage2.GetComponent<StageBgm>().playBackGroundMusic();

			if(Emitter.stageClear)
			{
				DestroyPlayer();

				stage2.GetComponent<StageEmitter>().DestroyWave();

				stage2.GetComponent<StageEmitter>().DestroySideEnemy();

				stage2.GetComponent<StageBgm>().stopBackGroundMusic();

				stage2.SetActive(false);
				
				result.SetActive(true);

				fade.GetComponent<FadeTexture>().startFadeOut();

				m_step = (int)kSceneState.Scene_Result;
			}
			break;
		case (int)kSceneState.Scene_Result:

			Emitter.stageClear = false;

			if(SceneResult.isNextStage)
			{	
				result.SetActive(false);
				
				SceneResult.isNextStage = false;

				m_step = (int)kSceneState.Scene_Stage2;

				stage2.GetComponent<StageEmitter>().ReswapnWave();

				stage2.GetComponent<StageEmitter>().ReswapnSideEnemy();
				
			}else if(gameClearFlag){	

				fade.GetComponent<FadeTexture>().startFadeIn();

				gameClear.SetActive(true);
			
				result.SetActive(false);
				
				gameClearFlag = false;

				m_time = 0.0f;

				m_step = (int)kSceneState.Scene_GameClear;

			}
			break;
		case (int)kSceneState.Scene_GameOver:

			if(stage2.activeSelf)
			{
				stage2.GetComponent<StageEmitter>().DestroyWave();
				stage2.GetComponent<StageEmitter>().DestroySideEnemy();
				stage2.SetActive(false);
				fade.GetComponent<FadeTexture>().startFadeOut();
			}

			if(stage1.activeSelf)
			{
				stage1.GetComponent<StageEmitter>().DestroyWave();
				stage1.SetActive(false);
				fade.GetComponent<FadeTexture>().startFadeOut();
			}

			if(Input.GetKeyDown( KeyCode.X ))
			{
				gameOver.SetActive(false);
				m_step = (int)kSceneState.Scene_Title;

				//Reload this stage.unity
				Application.LoadLevel(Application.loadedLevel);
			}

			break;
		case (int)kSceneState.Scene_GameClear:

			if(Input.GetKeyDown( KeyCode.X ))
			{
				gameClear.SetActive(false);
				m_step = (int)kSceneState.Scene_Title;

				//Reload this stage.unity
				Application.LoadLevel(Application.loadedLevel);
			}

			break;
		default:
			break;
		}
	}

	public void StepAdd()
	{
		m_step += 1;
	}
		
	public void GameOver ()
	{
		FindObjectOfType<Score>().Save();
		// ゲームオーバー時に、タイトルを表示する
		gameOver.SetActive (true);

		m_step = (int)kSceneState.Scene_GameOver;
	}

	private void Fade()
	{
		m_time += Time.deltaTime * 0.25f;
	
		if(m_time >= 1.0f)
		{
			FadeInLogo();
			FadeInTexture();
			StepAdd();
			m_time = 0.0f;
		}

	}

	private void FadeInLogo()
	{
		fadeLogo.GetComponent<ImageLogo>().startFadeIn();
	}
	
	private void FadeInTexture()
	{
		fade.GetComponent<FadeTexture>().startFadeIn();
	}

	//	キャラクター作成
	public void CreatePlayer()
	{
		if (m_create_char == 1) {
			m_marika.GetComponent<Player> ().InstantiatePlayer ();
		}
		else if(m_create_char == 2) {
			m_jack.GetComponent<Jack> ().InstantiatePlayer ();
		}
	}

	//	デストロイ
	public void DestroyPlayer()
	{
		if (m_create_char == 1) {
			Destroy(GameObject.Find("Hummer(Clone)"));
			Destroy(GameObject.Find("Marika(Clone)"));
			Destroy(GameObject.Find("MarikaNotDamColAre(Clone)"));
		}

		if (m_create_char == 2) {
			Destroy(GameObject.Find("Jack(Clone)"));
			Destroy(GameObject.Find("Punch(Clone)"));
			Destroy(GameObject.Find("JackNotDamColAre(Clone)"));
		}
	}

	public void PressedStartInTitle()
	{
		m_startTitle = true;
	}

	public void CreateFlagSet(int num)
	{
		m_create_char = num;
	}
}