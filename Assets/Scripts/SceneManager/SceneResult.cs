﻿using UnityEngine;
using System.Collections;

public class SceneResult : MonoBehaviour {
	
	static public bool isNextStage = false;
	
	private string kLastStageObjectName = "SceneStage2";
	
	public void ChangeStage()
	{
		if(Emitter.stageName != kLastStageObjectName)
		{
			isNextStage = true;
		}else if(Emitter.stageName == kLastStageObjectName){
			SceneManager.gameClearFlag = true;
		}
	}
}
