﻿using UnityEngine;
using System.Collections;

public class StageEmitter : MonoBehaviour {

	public GameObject emitter;
	public GameObject sideEnemy;

	private GameObject clone;
	private GameObject sideClone;

	public void ReswapnWave()
	{
		clone = (GameObject)Instantiate (emitter, emitter.transform.position, emitter.transform.rotation);
		
		//Set emitter as a child object
		clone.transform.parent = this.transform;
	}

	public void DestroyWave()
	{
		DestroyImmediate(clone,true);
	}

	public void ReswapnSideEnemy()
	{
		if(sideEnemy)
		{
			sideClone = (GameObject)Instantiate (sideEnemy, sideEnemy.transform.position, sideEnemy.transform.rotation);

			sideClone.transform.parent = this.transform;
		}
	}

	public void DestroySideEnemy()
	{
		//DestroyImmediate(sideClone,true);
		Destroy(sideClone);
	}
}
