﻿ using UnityEngine;

[RequireComponent(typeof(Rigidbody2D), typeof(Animator))]
public class Spaceship : MonoBehaviour
{
	// 移動スピード
	public float speed;
	
	// 弾を撃つ間隔
	public float shotDelay;
	
	// 弾のPrefab
	public GameObject bullet;
	// 弾のPrefab
	//public GameObject bullet_col;
	
	// 弾を撃つかどうか
	public bool canShot;
	
	// 爆発のPrefab
	public GameObject explosion;

	//	体力
	public int life;

	// アニメーターコンポーネント
	private Animator animator;

	//
	private AudioSource audioSource;
	public AudioClip Damaged;
	
	void Start ()
	{
		//
		audioSource = gameObject.GetComponent<AudioSource>();

		// アニメーターコンポーネントを取得
		animator = GetComponent<Animator> ();
	}


	//	ダメージ・体力判定
	public bool LifeCheck( int damege )
	{
		//	ダメージ
		life -= damege;
		//	判定
		if (life <= 0)return false;
		else return true;
	}

	// 爆発の作成
	public void Explosion ()
	{
		Instantiate (explosion, transform.position, transform.rotation);
	}
	
	// 弾の作成
	public void Shot (Transform origin)
	{
		Instantiate (bullet, origin.position, origin.rotation);
		/*if( Random.value < 0.8f )
		{
			Instantiate (bullet, origin.position, origin.rotation);
		}
		else {*/
			//Instantiate (bullet_col, origin.position, origin.rotation);

	}

	// アニメーターコンポーネントの取得
	public Animator GetAnimator()
	{
		return animator;
	}

	public void PlaySoundDamaged()
	{
		//
		audioSource.PlayOneShot( Damaged );
	}
}