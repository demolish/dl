﻿using UnityEngine;
using System.Collections;

public class EnemyFollow : MonoBehaviour {
		// ヒットポイント
		public int hp = 1;
		
		// スコアのポイント
		public int point = 100;
		
		// Spaceshipコンポーネント
		Spaceship spaceship;

		GameObject player;


		float turnSpeed=4.0f;
		Vector3 moveDirection;
		Vector3 currentPosition;
		
		//
		public GameObject min_enemy_damaged_effect;
		public GameObject max_enemy_damaged_effect;
		public GameObject destried_effect;
		
		// 
		private bool fOpen = true;
		
		IEnumerator Start ()
		{
			
		     
			// Spaceshipコンポーネントを取得
			spaceship = GetComponent<Spaceship> ();
			//player
			//player = GameObject.Find("Marika(Clone)");
			if( SceneManager.m_create_char == 1)
			{
				player = GameObject.Find("Marika(Clone)");
			}
			else if( SceneManager.m_create_char == 2)
			{
				player = GameObject.Find("Jack(Clone)");
			}	

			// canShotがfalseの場合、ここでコルーチンを終了させる
			if (spaceship.canShot == false) {
				yield break;
			}
			
			while (true) {
				
				// 子要素を全て取得する
				for (int i = 0; i < transform.childCount; i++) {
					
					Transform shotPosition = transform.GetChild (i);
					
					// ShotPositionの位置/角度で弾を撃つ
					spaceship.Shot (shotPosition);
				}
				
				// shotDelay秒待つ
				yield return new WaitForSeconds (spaceship.shotDelay);
			}
		}
		
		void Update()
    	{
			if (player) {
						float L = Vector3.Distance(transform.position,player.transform.position);
	
						if (L>=1.8f) {
							if(fOpen==true){FollowMove ();}
						}
				
						else if (L<1.8f) { 
								Move (transform.up * -1.0f * 1.8f);
								fOpen=false;
						}
						}
		}
		
		void FollowMove()
     	{
            currentPosition = transform.position;

			Vector3 moveToward = player.transform.position;
			moveDirection = moveToward - currentPosition;
			moveDirection.z = 0;
			moveDirection.Normalize ();
			Vector3 target = moveDirection *spaceship.speed + currentPosition;
			transform.position = Vector3.Lerp (currentPosition, target, Time.deltaTime);

            float targetAngle = Mathf.Atan2(moveDirection.y, moveDirection.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 0, targetAngle + 90.0f), turnSpeed * Time.deltaTime);
	    }

		private Vector2 Follow()
		{
            //直线
			Vector2 follow;
			follow.x = 0.0f;
			follow.y = 0.0f;
			if (player != null) {
						float x = this.transform.position.x - (player.transform.position.x-1.0f);
						float y = this.transform.position.y - (player.transform.position.y+1.0f);
						if (x > 0 && y < 0) {
								follow.x = 1.0f;
								follow.y = 1.0f;
						}
						if (x < 0 && y < 0) {
								follow.x = -1.0f;
								follow.y = 1.0f;
						}
						if (x < 0 && y > 0) {
								follow.x = -1.0f;
								follow.y = 1.0f;
						}
						if (x >= 0 && y >= 0) {
								follow.x = 1.0f;
								follow.y = 1.0f;
						}
				}
		return follow;

		}
		// 機体の移動
		public void Move (Vector2 direction)
		{
		//rigidbody2D.velocity = new Vector2 (direction.x * spaceship.speed*1.3f,direction.y * spaceship.speed);
		rigidbody2D.velocity = direction * spaceship.speed;
		}
		
	void OnTriggerEnter2D (Collider2D c)
	{
		// 
		string layerName = LayerMask.LayerToName (c.gameObject.layer);
		
		// 
		if (layerName == "Bullet (Enemy)")return;
		if (layerName != "Bullet (Player)" && layerName != "Punch(Jack)") return;
		
		// 
		Transform playerBulletTransform = c.transform;
		
		if (layerName == "Bullet (Player)" )
		{
			spaceship.PlaySoundDamaged();

			Hummer bullet = playerBulletTransform.GetComponent<Hummer>();
			hp = hp - bullet.power;
			if( bullet.CheckNormalShot() ){
				Instantiate( max_enemy_damaged_effect, transform.position, transform.rotation );
			}
			//else{ Instantiate( min_enemy_damaged_effect, transform.position, transform.rotation ); }
		}
		
		if( layerName == "Punch(Jack)" )
		{
			spaceship.PlaySoundDamaged();

			Punch bullet = playerBulletTransform.GetComponent<Punch>();
			
			hp = hp - bullet.power;
			
			
			if( bullet.CheckNormalPunch() ){
				Instantiate( max_enemy_damaged_effect, transform.position, transform.rotation );
			}
			else { Instantiate( min_enemy_damaged_effect, transform.position, transform.rotation ); }
		}
		
		
		if(hp <= 0 )
		{		
			//FindObjectOfType<Score>().AddPoint(point);
			
			spaceship.Explosion();
			
			Instantiate( destried_effect, transform.position, transform.rotation );
			
			Destroy (gameObject);
		}else{
			spaceship.GetAnimator().SetTrigger("Damage");	
		}
	}
}