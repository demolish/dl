﻿using UnityEngine;
using System.Collections;


public class Boss1 : MonoBehaviour
{
	// ヒットポイント
	public int hp = 1;
	
	// スコアのポイント
	public int point = 100;

	private int step=0;

	//
	public GameObject min_enemy_damaged_effect;
	public GameObject max_enemy_damaged_effect;
	public GameObject destried_effect;


	// Spaceshipコンポーネント
	Spaceship spaceship;


	IEnumerator Start ()
	{
		
		// Spaceshipコンポーネントを取得
		spaceship = GetComponent<Spaceship> ();
		
		// canShotがfalseの場合、ここでコルーチンを終了させる
		if (spaceship.canShot == false) {
			yield break;
		}
		
		while (true) {
			
			// 子要素を全て取得する
			for (int i = 0; i < transform.childCount; i++) {
				
				Transform shotPosition = transform.GetChild (i);
				
				// ShotPositionの位置/角度で弾を撃つ
				spaceship.Shot (shotPosition);
			}
			
			// shotDelay秒待つ
			yield return new WaitForSeconds (spaceship.shotDelay);
		}
	}
	void Update()
	{	
		if (this.transform.position.y <= 1.0f)this.transform.position = new Vector3(this.transform.position.x,1.0f,this.transform.position.z);
		else Move (transform.up * -1);

		ChangeRotateAndShotByAttacked();
	}
	
	void ChangeRotateAndShotByAttacked()
	{

		switch (step) {
		case 0:

			if (this.hp < 500&&transform.position.y==1.0f)step++;
			break;
		case 1:
			this.transform.Rotate (new Vector3 (0, 0, 30) * Time.deltaTime);
			if (this.hp < 450)step++; //right
			break;
		case 2:
			this.spaceship.shotDelay = 0.4f;
			this.transform.Rotate (new Vector3 (0, 0, -60) * Time.deltaTime);
			if (this.hp < 350)step++;//left
			break;
		case 3:
			this.spaceship.shotDelay = 0.3f;
			this.transform.Rotate (new Vector3 (0, 0, 60) * Time.deltaTime);
			if (this.hp < 250)step++;//right
			break;
		case 4:
			this.spaceship.shotDelay = 0.2f;
			this.transform.Rotate (new Vector3 (0, 0, -60) * Time.deltaTime);
			if (this.hp < 150)step++;//left
			break;
		case 5:
			this.spaceship.shotDelay = 0.1f;
			this.transform.Rotate (new Vector3 (0, 0, 60) * Time.deltaTime);
			break;
		}

	}
	
	// 機体の移動
	public void Move (Vector2 direction)
	{
		rigidbody2D.velocity = direction * spaceship.speed;
	}
	
	void OnTriggerEnter2D (Collider2D c)
	{
		// 
		string layerName = LayerMask.LayerToName (c.gameObject.layer);
		
		// 
		if (layerName == "Bullet (Enemy)")return;
		if (layerName != "Bullet (Player)" && layerName != "Punch(Jack)") return;
		
		// 
		Transform playerBulletTransform = c.transform;
		
		if (layerName == "Bullet (Player)" )
		{
			spaceship.PlaySoundDamaged();

			Hummer bullet = playerBulletTransform.GetComponent<Hummer>();
			hp = hp - bullet.power;
			if( bullet.CheckNormalShot() ){
				Instantiate( max_enemy_damaged_effect, transform.position, transform.rotation );
			}
			else{ Instantiate( min_enemy_damaged_effect, transform.position, transform.rotation ); }
		}
		
		if( layerName == "Punch(Jack)" )
		{
			spaceship.PlaySoundDamaged();

			Punch bullet = playerBulletTransform.GetComponent<Punch>();
			
			hp = hp - bullet.power;
			
			
			if( bullet.CheckNormalPunch() ){
				Instantiate( max_enemy_damaged_effect, transform.position, transform.rotation );
			}
			else { Instantiate( min_enemy_damaged_effect, transform.position, transform.rotation ); }
		}
		
		
		if(hp <= 0 )
		{		
			//FindObjectOfType<Score>().AddPoint(point);
			
			spaceship.Explosion();
			
			Instantiate( destried_effect, transform.position, transform.rotation );
			
			Destroy (gameObject);
		}else{
			spaceship.GetAnimator().SetTrigger("Damage");	
		}
	}
}