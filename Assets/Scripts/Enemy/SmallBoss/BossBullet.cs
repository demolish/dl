﻿using UnityEngine;

public class BossBullet : Bullet 
{

	// Use this for initialization
	void Start () {

		rigidbody2D.velocity = transform.up.normalized * speed;
		
		// lifeTime秒後に削除
		Destroy (gameObject, lifeTime);
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
