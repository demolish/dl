﻿using UnityEngine;
using System.Collections;

public class EnemyDownShotUp : MonoBehaviour
{
    // ヒットポイント
    public int hp = 1;

    // スコアのポイント
    public int point = 100;

	public float high=1.0f;
   
	// Spaceshipコンポーネント
    Spaceship spaceship;
	int step=0;
	float time=0.0f;
	public bool Modoru;

    void Start()
    {
        // Spaceshipコンポーネントを取得
        spaceship = GetComponent<Spaceship>();
    }

	void Update(){
		Move ();
	}

	void Move()
	{
		switch (step) {
		case 0:
			Down (transform.up * -1);
			step++;
			break;
		case 1:
		if(this.transform.position.y<=high){
			this.transform.position = new Vector3 (this.transform.position.x, high, this.transform.position.z);
			time = time + Time.deltaTime;
			if(time>=1){
					time=0;

					StartCoroutine(Shot ());

					if(Modoru==false)break;
					if(Modoru==true){
						step++;}
				}
			}
			break;
		case 2:
			Down (transform.up * 1);
			break;
		}
	}
    // 機体の移動
    public void Down(Vector2 direction)
    {
        rigidbody2D.velocity = direction * spaceship.speed;

    }
	IEnumerator wait ()
	{
		yield return new WaitForSeconds(2.0f);
	}
	IEnumerator  Shot(){
		if (spaceship.canShot == false) {
			yield break;
		}
		
		while (true) {
			
			// 子要素を全て取得する
			for (int i = 0; i < transform.childCount; i++) {
				
				Transform shotPosition = transform.GetChild (i);
				
				// ShotPositionの位置/角度で弾を撃つ
				spaceship.Shot (shotPosition);

			}
			
			// shotDelay秒待つ
			yield return new WaitForSeconds (spaceship.shotDelay);
			yield break;

		}
	}

	//	被弾時のエフェクト
	public GameObject min_enemy_damaged_effect;
	public GameObject max_enemy_damaged_effect;
	//	被撃破時のエフェクト
	public GameObject destried_effect;

	void OnTriggerEnter2D (Collider2D c)
	{
		// レイヤーネーム取得
		string layerName = LayerMask.LayerToName (c.gameObject.layer);
		
		// レイヤー名がBullet (Player)以外の時は何も行わない
		if (layerName == "Bullet (Enemy)")return;
		if (layerName != "Bullet (Player)" && layerName != "Punch(Jack)") return;
		
		// PlayerBulletのTransformを取得
		Transform playerBulletTransform = c.transform;
		
		if (layerName == "Bullet (Player)" )
		{
			spaceship.PlaySoundDamaged();

			// Bulletコンポーネントを取得
			Hummer bullet = playerBulletTransform.GetComponent<Hummer>();
			// ヒットポイントを減らす
			hp = hp - bullet.power;
			
			//	ノーマルショットの場合強ダメージEffect
			if( bullet.CheckNormalShot() ){
				Instantiate( max_enemy_damaged_effect, transform.position, transform.rotation );
			}
			else { Instantiate( min_enemy_damaged_effect, transform.position, transform.rotation ); }
		}	
		
		if( layerName == "Punch(Jack)" )
		{
			spaceship.PlaySoundDamaged();

			// Bulletコンポーネントを取得
			Punch bullet = playerBulletTransform.GetComponent<Punch>();
			// ヒットポイントを減らす
			hp = hp - bullet.power;
			
			//	ノーマルパンチの場合強ダメージEffect
			if( bullet.CheckNormalPunch() ){
				Instantiate( max_enemy_damaged_effect, transform.position, transform.rotation );
			}
			else { Instantiate( min_enemy_damaged_effect, transform.position, transform.rotation ); }
		}
		
		//	生存確認
		if(hp <= 0 )
		{
			//	スコア加算
			//FindObjectOfType<Score>().AddPoint(point);
			
			// 爆発アニメーション
			spaceship.Explosion();
			//
			Instantiate( destried_effect, transform.position, transform.rotation );
			
			// 削除
			Destroy (gameObject);
		}else{
			// ダメージエフェクト
			spaceship.GetAnimator().SetTrigger("Damage");	
		}
	}
}