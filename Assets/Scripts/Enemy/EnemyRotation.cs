using UnityEngine;
using System.Collections;

public class EnemyRotation : MonoBehaviour
{
	public int hp = 1;

	public int point = 100;

	//
	public GameObject min_enemy_damaged_effect;
	public GameObject max_enemy_damaged_effect;
	public GameObject destried_effect;

	public bool rotation = false;

	Spaceship spaceship;
	
	IEnumerator Start ()
	{
		spaceship = GetComponent<Spaceship> ();

		Move (transform.up * -1);

		if (spaceship.canShot == false) 
		{
			yield break;
		}
		
		while (true) 
		{
			for (int i = 0; i < transform.childCount; i++) 
			{			
				Transform shotPosition = transform.GetChild (i);

				spaceship.Shot (shotPosition);
			}
			yield return new WaitForSeconds (spaceship.shotDelay);
		}
	}
	
	void Update()
	{
		if(rotation==false)transform.Rotate(new Vector3(0, 0, 90) * Time.deltaTime);
		else transform.Rotate(new Vector3(0, 0, -90) * Time.deltaTime);
	}

	public void Move (Vector2 direction)
	{
		rigidbody2D.velocity = direction * spaceship.speed;
	}
	
	void OnTriggerEnter2D (Collider2D c)
	{
		// 
		string layerName = LayerMask.LayerToName (c.gameObject.layer);
		
		// 
		if (layerName == "Bullet (Enemy)")return;
		if (layerName != "Bullet (Player)" && layerName != "Punch(Jack)") return;
		
		// 
		Transform playerBulletTransform = c.transform;

		if (layerName == "Bullet (Player)" )
		{
			spaceship.PlaySoundDamaged();

			Hummer bullet = playerBulletTransform.GetComponent<Hummer>();
			hp = hp - bullet.power;
			if( bullet.CheckNormalShot() ){
				Instantiate( max_enemy_damaged_effect, transform.position, transform.rotation );
			}
			else{ Instantiate( min_enemy_damaged_effect, transform.position, transform.rotation ); }
		}

		if( layerName == "Punch(Jack)" )
		{
			spaceship.PlaySoundDamaged();

			Punch bullet = playerBulletTransform.GetComponent<Punch>();

			hp = hp - bullet.power;


			if( bullet.CheckNormalPunch() ){
				Instantiate( max_enemy_damaged_effect, transform.position, transform.rotation );
			}
			else { Instantiate( min_enemy_damaged_effect, transform.position, transform.rotation ); }
		}
	

		if(hp <= 0 )
		{		
			//FindObjectOfType<Score>().AddPoint(point);

			spaceship.Explosion();

			Instantiate( destried_effect, transform.position, transform.rotation );

			Destroy (gameObject);
		}else{
			spaceship.GetAnimator().SetTrigger("Damage");	
		}
	}
}