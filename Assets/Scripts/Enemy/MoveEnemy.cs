﻿using UnityEngine;
using System.Collections;

public class MoveEnemy : MonoBehaviour 
{
	//	移動座標
	public Vector2[] GoalPos;
	//	目標座標到達後の待機時間(オリジナル)
	public int wait_time_org;
	
	//	目標座標到達後の待機時間
	private int wait_time;
	//	目標座標インデックス
	private int index_goal = 0;
	//	目標までの距離
	private float goal_length;
	//	配列の長さ
	private int goal_num;
	//	自分の座標
	private Vector2 my_pos;
	//	移動方向
	private Vector2 move_angle;

	//	被弾時のエフェクト
	public GameObject min_enemy_damaged_effect;
	public GameObject max_enemy_damaged_effect;
	//	被撃破時のエフェクト
	public GameObject destried_effect;

	// 体力
	public int hp = 10;
	// スコア
	public int point = 100;

	Spaceship spaceship;
	
	void Start ()
	{
		// Spaceship
		spaceship = GetComponent<Spaceship> ();
		//	長さ取得
		goal_num = GoalPos.Length;
		//	待機時間設定
		wait_time_org *= 60;
		wait_time = wait_time_org;
	}
	
	void Update()
	{
		//	自分の座標更新
		my_pos.x = transform.position.x;
		my_pos.y = transform.position.y;
		
		// 移動方向・距離決定
		CheckMoveAngle ();
		//	目標座標チェック
		CheckGoal();
		
	}
	
	
	
	//	目標座標チェック
	//	TRUE値：　　そのまま更新
	//	FALSE値；　 目標座標終了
	void CheckGoal()
	{
		//	距離チェック	
		if ( goal_length > 0.01f ){ 
			//	移動
			Move ();
		}
		else
		{ 
			rigidbody2D.velocity = new Vector2(0,0);
			//	待機時間判定
			if( wait_time <= 0 )
			{
				//	インデックス更新
				if( goal_num >= index_goal + 2 ){ 
					//	更新
					index_goal++;
					//	待機時間設定
					wait_time = wait_time_org; 
				}
			}
			wait_time--;
		}
	}
	
	//	移動方向決定
	void CheckMoveAngle(){
		//向き設定
		move_angle = GoalPos [index_goal] - my_pos;
		//	目標までの距離算出
		goal_length = move_angle.magnitude;
		//	正規化
		move_angle.Normalize ();
	}
	
	// 移動
	public void Move (){
		rigidbody2D.velocity = move_angle * spaceship.speed;
	}
	
	void OnTriggerEnter2D (Collider2D c)
	{
		// レイヤーネーム取得
		string layerName = LayerMask.LayerToName (c.gameObject.layer);
		
		// レイヤー名がBullet (Player)以外の時は何も行わない
		if (layerName == "Bullet (Enemy)")return;
		if (layerName != "Bullet (Player)" && layerName != "Punch(Jack)") return;
		
		// PlayerBulletのTransformを取得
		Transform playerBulletTransform = c.transform;
		
		if (layerName == "Bullet (Player)" )
		{
			spaceship.PlaySoundDamaged();

			// Bulletコンポーネントを取得
			Hummer bullet = playerBulletTransform.GetComponent<Hummer>();
			// ヒットポイントを減らす
			hp = hp - bullet.power;

			//	ノーマルショットの場合強ダメージEffect
			if( bullet.CheckNormalShot() ){
				Instantiate( max_enemy_damaged_effect, transform.position, transform.rotation );
			}
			else { Instantiate( min_enemy_damaged_effect, transform.position, transform.rotation ); }
		}	
		
		if( layerName == "Punch(Jack)" )
		{
			spaceship.PlaySoundDamaged();

			// Bulletコンポーネントを取得
			Punch bullet = playerBulletTransform.GetComponent<Punch>();
			// ヒットポイントを減らす
			hp = hp - bullet.power;

			//	ノーマルパンチの場合強ダメージEffect
			if( bullet.CheckNormalPunch() ){
				Instantiate( max_enemy_damaged_effect, transform.position, transform.rotation );
			}
			else { Instantiate( min_enemy_damaged_effect, transform.position, transform.rotation ); }
		}
		
		//	生存確認
		if(hp <= 0 )
		{
			//	スコア加算
			FindObjectOfType<Score>().AddPoint(point);
			
			// 爆発アニメーション
			spaceship.Explosion();
			//
			Instantiate( destried_effect, transform.position, transform.rotation );
			
			// 削除
			Destroy (gameObject);
		}else{
			// ダメージエフェクト
			spaceship.GetAnimator().SetTrigger("Damage");	
		}
	}
}
