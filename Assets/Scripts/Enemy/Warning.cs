﻿using UnityEngine;
using System.Collections;

public class Warning : MonoBehaviour {
	
	Animator anim;
	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {

		Destroy(gameObject,5.0f);

	}
	void playAnime(){
		anim.Play("warning");
	}
}
