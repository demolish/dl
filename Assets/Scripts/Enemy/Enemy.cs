﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour
{
	// ヒットポイント
	public int hp = 1;

	// スコアのポイント
	public int point = 100;

	// Spaceshipコンポーネント
	Spaceship spaceship;

	//	被弾時のエフェクト
	public GameObject min_enemy_damaged_effect;
	public GameObject max_enemy_damaged_effect;
	//	被弾時のエフェクト
	public GameObject destried_effect;

	IEnumerator Start ()
	{
		// Spaceshipコンポーネントを取得
		spaceship = GetComponent<Spaceship> ();
		
		// ローカル座標のY軸のマイナス方向に移動する
		Move (transform.up * -1);
		
		// canShotがfalseの場合、ここでコルーチンを終了させる
		if (spaceship.canShot == false) {
			yield break;
		}
			
		while (true) {
			
			// 子要素を全て取得する
			for (int i = 0; i < transform.childCount; i++) {
				
				Transform shotPosition = transform.GetChild (i);
				
				// ShotPositionの位置/角度で弾を撃つ
				spaceship.Shot (shotPosition);
			}
			
			// shotDelay秒待つ
			yield return new WaitForSeconds (spaceship.shotDelay);
		}
	}

	// 機体の移動
	public void Move (Vector2 direction)
	{
		rigidbody2D.velocity = direction * spaceship.speed;
	}

	void OnTriggerEnter2D (Collider2D c)
	{
		// レイヤーネーム取得
		string layerName = LayerMask.LayerToName (c.gameObject.layer);
		
		// レイヤー名がBullet (Player)以外の時は何も行わない
		if (layerName == "Bullet (Enemy)")return;
		if (layerName != "Bullet (Player)" && layerName != "Punch(Jack)") return;
		
		// PlayerBulletのTransformを取得
		Transform playerBulletTransform = c.transform;
		
		if (layerName == "Bullet (Player)" )
		{
			spaceship.PlaySoundDamaged();

			// Bulletコンポーネントを取得
			Hummer bullet = playerBulletTransform.GetComponent<Hummer>();
			// ヒットポイントを減らす
			hp = hp - bullet.power;
			
			//	ノーマルショットの場合強ダメージEffect
			if( bullet.CheckNormalShot() ){
				Instantiate( max_enemy_damaged_effect, transform.position, transform.rotation );
			}
			else { Instantiate( min_enemy_damaged_effect, transform.position, transform.rotation ); }
		}	
		
		if( layerName == "Punch(Jack)" )
		{
			spaceship.PlaySoundDamaged();

			// Bulletコンポーネントを取得
			Punch bullet = playerBulletTransform.GetComponent<Punch>();
			// ヒットポイントを減らす
			hp = hp - bullet.power;
			
			//	ノーマルパンチの場合強ダメージEffect
			if( bullet.CheckNormalPunch() ){
				Instantiate( max_enemy_damaged_effect, transform.position, transform.rotation );
			}
			else { Instantiate( min_enemy_damaged_effect, transform.position, transform.rotation ); }
		}
		
		//	生存確認
		if(hp <= 0 )
		{
			//	スコア加算
			//FindObjectOfType<Score>().AddPoint(point);
			
			// 爆発アニメーション
			spaceship.Explosion();
			//
			Instantiate( destried_effect, transform.position, transform.rotation );
			
			// 削除
			Destroy (gameObject);
		}else{
			// ダメージエフェクト
			spaceship.GetAnimator().SetTrigger("Damage");	
		}
	}
}