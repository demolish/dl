﻿using UnityEngine;
using System.Collections;

public class EnemyBulletRotate : MonoBehaviour {

	GameObject player;

	float turnSpeed=8.0f;
	Vector3 moveDirection;
	Vector3 currentPosition;
	// Use this for initialization
	void Start () {
		if( SceneManager.m_create_char == 1)
			{
				player = GameObject.Find("Marika(Clone)");
			}
			else if( SceneManager.m_create_char == 2)
			{
				player = GameObject.Find("Jack(Clone)");
			}	

	}
	
	// Update is called once per frame
	void Update () {
		currentPosition = transform.position;
		
		Vector3 moveToward = player.transform.position;
		moveDirection = moveToward - currentPosition;
		moveDirection.z = 0;
		moveDirection.Normalize ();
		Vector3 target = moveDirection *0 + currentPosition;
		transform.position = Vector3.Lerp (currentPosition, target, Time.deltaTime*0.8f);
		
		float targetAngle = Mathf.Atan2(moveDirection.y, moveDirection.x) * Mathf.Rad2Deg;
		transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 0, targetAngle + 270.0f), turnSpeed * Time.deltaTime);
	}
}
