﻿using UnityEngine;
using System.Collections;

public class EnemyDownLeftRightUpShot : MonoBehaviour
{
	// ヒットポイント
	public int hp = 1;
	
	// スコアのポイント
	public int point = 100;
	
	// Spaceshipコンポーネント
	Spaceship spaceship;
	int step=0;
	float time=0.0f;
	float shotTime=0.0f;
	IEnumerator Start()
	{
		// Spaceshipコンポーネントを取得
		spaceship = GetComponent<Spaceship>();
		// ローカル座標のY軸のマイナス方向に移動する
		//   Down(transform.up * -1);
		yield break;
		// canShotがfalseの場合、ここでコルーチンを終了させる
		/*
		*/
	}
	void Update(){
		//Down (transform.up * -1);
		Move ();
		
	}
	void  Shot(){
		if (spaceship.canShot == false)
		{
			//break;
		}
		
		/*if (spaceship.canShot == true)
		{
				// 子要素を全て取得する
			for (int i = 0; i < transform.childCount; i++)
			{
						
			Transform shotPosition = transform.GetChild(i);
						
				// ShotPositionの位置/角度で弾を撃つ
			spaceship.Shot(shotPosition);

			}*/
			// shotDelay秒待つ
			while (spaceship.canShot == true) {
						shotTime = shotTime + Time.deltaTime * 0.1f;
						if (shotTime >= spaceship.shotDelay) {
						for (int i = 0; i < transform.childCount; i++)
							{
								Transform shotPosition = transform.GetChild(i);
					
								// ShotPositionの位置/角度で弾を撃つ
								spaceship.Shot(shotPosition);
							}
								time = 0;

						}
				}
		}
	void Move()
	{
		switch (step) {
		case 0:
			Down (transform.up * -1);
			if(this.transform.position.y<=1.0f){
				this.transform.position = new Vector3 (this.transform.position.x, 1.0f, this.transform.position.z);
				time = time + Time.deltaTime;
				if(time>=1){
					time=0;
					Shot ();
					step++;
				}
			}
			break;
		case 1:
			//if(this.transform.position.y<1.5f){

			break;
			//}
			/*if(this.transform.position.y>=1.5f){
				this.transform.position = new Vector3 (this.transform.position.x, 1.5f, this.transform.position.z);
				time = time + Time.deltaTime;
				if(time>=1){
					time=0;
					Shot ();
					step++;
				}

			}*/

		}
	}
	// 機体の移動
	void Right()
	{

	}
	public void Down(Vector2 direction)
	{
		rigidbody2D.velocity = direction * spaceship.speed;
		
	}
	
	void OnTriggerEnter2D(Collider2D c)
	{
		// レイヤー名を取得
		string layerName = LayerMask.LayerToName(c.gameObject.layer);
		
		// レイヤー名がBullet (Player)以外の時は何も行わない
		if (layerName != "Bullet (Player)")
		{
			return;
		}
		
		// PlayerBulletのTransformを取得
		Transform playerBulletTransform = c.transform;
		
		// Bulletコンポーネントを取得
		Hummer bullet = playerBulletTransform.GetComponent<Hummer>();
		
		// ヒットポイントを減らす
		hp = hp - bullet.power;
		
		// ヒットポイントが0以下であれば
		if (hp <= 0)
		{
			// スコアコンポーネントを取得してポイントを追加
			FindObjectOfType<Score>().AddPoint(point);
			
			// 爆発
			spaceship.Explosion();
			
			// エネミーの削除
			Destroy(gameObject);
			
		}
		else
		{
			// Damageトリガーをセット
			spaceship.GetAnimator().SetTrigger("Damage");
			
		}
	}
}
