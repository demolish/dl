﻿using UnityEngine;
using System.Collections;

public class RocketShoter : MonoBehaviour {

	// ヒットポイント
	public int hp = 1;
	
	// スコアのポイント
	public int point = 100;
	
	// Spaceshipコンポーネント
	Spaceship spaceship;
	
	//	被弾時のエフェクト
	public GameObject enemy_damaged_effect;
	
	IEnumerator Start ()
	{
		// Spaceshipコンポーネントを取得
		spaceship = GetComponent<Spaceship> ();
		
		// ローカル座標のY軸のマイナス方向に移動する
		Move (transform.up * -1);
		
		// canShotがfalseの場合、ここでコルーチンを終了させる
		if (spaceship.canShot == false) {
			yield break;
		}
		
		while (true) {
			
			// 子要素を全て取得する
			for (int i = 0; i < transform.childCount; i++) {
				
				Transform shotPosition = transform.GetChild (i);
				
				// ShotPositionの位置/角度で弾を撃つ
				spaceship.Shot (shotPosition);
			}
			
			// shotDelay秒待つ
			yield return new WaitForSeconds (spaceship.shotDelay);
		}
	}
	void Update()
	{
		MoveToCenter();
		}
	// 機体の移動
	public void Move (Vector2 direction)
	{
		rigidbody2D.velocity = direction * spaceship.speed;
	}
	void MoveToCenter()
	{
		if (this.transform.position.y <= 5.0f) this.transform.position = new Vector3(this.transform.position.x, 5.0f, this.transform.position.z);
		else Move(transform.up * -1);
	}
	void OnTriggerEnter2D (Collider2D c)
	{
		// レイヤー名を取得
		string layerName = LayerMask.LayerToName (c.gameObject.layer);
		
		// レイヤー名がBullet (Player)以外の時は何も行わない
		if (layerName != "Bullet (Player)" && layerName != "Punch(Jack)") return;		
		
		// PlayerBulletのTransformを取得
		Transform playerBulletTransform = c.transform;
		
		if (layerName == "Bullet (Player)" )
		{
			spaceship.PlaySoundDamaged();

			// Bulletコンポーネントを取得
			Hummer bullet =  playerBulletTransform.GetComponent<Hummer>();
			// ヒットポイントを減らす
			hp = hp - bullet.power;
		}	
		
		if( layerName == "Punch(Jack)" )
		{
			spaceship.PlaySoundDamaged();

			Instantiate( enemy_damaged_effect, transform.position, transform.rotation );
			// Bulletコンポーネントを取得
			Punch bullet =  playerBulletTransform.GetComponent<Punch>();
			// ヒットポイントを減らす
			hp = hp - bullet.power;
		}
		
		// ヒットポイントが0以下であれば
		if(hp <= 0 )
		{
			// スコアコンポーネントを取得してポイントを追加
			FindObjectOfType<Score>().AddPoint(point);
			
			// 爆発
			spaceship.Explosion ();
			
			// エネミーの削除
			Destroy (gameObject);
			
		}else{
			// Damageトリガーをセット
			spaceship.GetAnimator().SetTrigger("Damage");
			
		}
	}
}