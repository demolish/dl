﻿using UnityEngine;
using System.Collections;
public class BigBoss : MonoBehaviour
{
	float time=0.0f;
	public int point = 100;
	public int hp;
	//
	public GameObject min_enemy_damaged_effect;
	public GameObject max_enemy_damaged_effect;
	public GameObject destried_effect;
    //段階
    int stage;
    //skill再起動準備時間
    float mainCD=8.0f;
	float MawaruCD=8.0f;
	float RocketCD=8.0f;
	float SideCD=8.0f;
	int dangerLine=0;
	BossShip spaceship;
	GameObject player;
	GameObject player2;
	GameObject left;
	GameObject right;
	Animator anim;
	Animator animL;
	Animator animR;

	bool rocketOpen = true;
    void Start()
	{
		player = GameObject.Find("Marika(Clone)");
		player2 = GameObject.Find("Jack(Clone)");
		left=GameObject.Find("boss_left");
		right=GameObject.Find("boss_right");
		spaceship = GetComponent<BossShip>();
		anim = GetComponent<Animator>();
		animL = left.GetComponent<Animator> ();
		animR = right.GetComponent<Animator> ();

		dangerLine = hp / 5;
    }

    void Update()
    {
        MoveToCenter();
		//Debug.Log("321312");
		mainCD = mainCD - Time.deltaTime;
		MawaruCD=MawaruCD-Time.deltaTime;
		RocketCD=RocketCD-Time.deltaTime;
		SideCD=SideCD-Time.deltaTime;
		switch (stage) {
		case 0: 
			stage0();
			if(left==null&&right==null)
			{
				stage++;
				//Debug.Log("++++");
			}
			break;
		case 1:
			//stage0 ();
			//Railgun ();
			if(MawaruCD<=0.0f){
			Mawaru();
			}
			if(RocketCD<=0.0f && rocketOpen == true){
				Rocket();
			}
			if(SideCD<=0.0f){
				//Debug.Log("jjjjjjjjj");
				//SideL();
				//SideR();

			}
			break;
		
		}
    }

    //	ダメージ・体力判定
    public bool LifeCheck(int damege)
    {
        //	ダメージ
        hp -= damege;
        //	判定
        if (hp <= 0) return false;
        else return true;
    }
    // 機体の移動
    public void Move(Vector2 direction)
    {
        rigidbody2D.velocity = direction * spaceship.speed;
    }
    //機体が中心に移動
    void MoveToCenter()
    {
        if (this.transform.position.y <= 2.0f) this.transform.position = new Vector3(this.transform.position.x, 2.0f, this.transform.position.z);
        else Move(transform.up * -1);
    }

	void OnTriggerEnter2D (Collider2D c)
	{
		// 
		string layerName = LayerMask.LayerToName (c.gameObject.layer);
		
		// 
		if (layerName == "Bullet (Enemy)")return;
		if (layerName != "Bullet (Player)" && layerName != "Punch(Jack)") return;
		
		// 
		Transform playerBulletTransform = c.transform;
		
		if (layerName == "Bullet (Player)" )
		{
			Hummer bullet = playerBulletTransform.GetComponent<Hummer>();
			hp = hp - bullet.power;
			if( bullet.CheckNormalShot() ){
				Instantiate( max_enemy_damaged_effect, transform.position, transform.rotation );
			}
			else{ Instantiate( min_enemy_damaged_effect, transform.position, transform.rotation ); }
		}
		
		if( layerName == "Punch(Jack)" )
		{
			
			Punch bullet = playerBulletTransform.GetComponent<Punch>();
			
			hp = hp - bullet.power;
			
			
			if( bullet.CheckNormalPunch() ){
				Instantiate( max_enemy_damaged_effect, transform.position, transform.rotation );
			}
			else { Instantiate( min_enemy_damaged_effect, transform.position, transform.rotation ); }
		}
		
		
		if(hp <= 0 )
		{		
			//FindObjectOfType<Score>().AddPoint(point);
			
			spaceship.Explosion();
			
			Instantiate( destried_effect, transform.position, transform.rotation );
			
			Destroy (gameObject);
		}else{
			spaceship.GetAnimator().SetTrigger("Damage");	
		}
	}

	void stage0()
	{

				if (this.transform.position.y <= 2.0f) {
						this.transform.position = new Vector3 (this.transform.position.x, 2.0f, this.transform.position.z);
						time = time + Time.deltaTime;
						if (time >= 1) {
								time = 0;
								Transform shotPosition1 = transform.FindChild ("Shot1_1");
								StartCoroutine (Shot (shotPosition1));
								Transform shotPosition2 = transform.FindChild ("Shot1_2");
								StartCoroutine (Shot (shotPosition2));
								Transform shotPosition3 = transform.FindChild ("Shot1_3");
								StartCoroutine (Shot (shotPosition3));
								Transform shotPosition4 = transform.FindChild ("Shot1_4");
								StartCoroutine (Shot (shotPosition4));
								Transform shotPosition5 = transform.FindChild ("Shot1_5");
								StartCoroutine (Shot (shotPosition5));
								Transform shotPosition6 = transform.FindChild ("Shot1_6");
								StartCoroutine (Shot (shotPosition6));
								Transform shotPosition7 = transform.FindChild ("Shot1_7");
								StartCoroutine (Shot (shotPosition7));
						}
				}
				if (mainCD <= 0.0f && player == true) {

						if (player.transform.position.x - this.transform.position.x <= 1.0f && player.transform.position.x - this.transform.position.x >= -1.0f) {
								Railgun ();
								mainCD = 1.0f;
						}
				}
				if (mainCD <= 0.0f && player2 == true) {
						if (player2.transform.position.x - this.transform.position.x <= 1.0f && player2.transform.position.x - this.transform.position.x >= -1.0f) {
								Railgun ();
								mainCD = 1.0f;
								
						} 
				}
		}
	IEnumerator  Shot(Transform shotPosition){
		if (spaceship.canShot == false) {
			yield break;
		}
		
		while (true) {
	
			// 子要素を全て取得する
		//	for (int i = 0; i < transform.childCount; i++) {
		//	shotPosition = transform.GetChild (i);
			spaceship.Shot (shotPosition);
		//	}
			// shotDelay秒待つ
			yield return new WaitForSeconds (spaceship.shotDelay);
			yield break;
		}
	}
	IEnumerator  ShotR(Transform shotPositionR){
		if (spaceship.canShot == false) {
			yield break;
		}
		
		while (true) {
			
			// 子要素を全て取得する
			//	for (int i = 0; i < transform.childCount; i++) {
			//	shotPosition = transform.GetChild (i);
			spaceship.ShotRed (shotPositionR);
			//	}
			// shotDelay秒待つ
			yield return new WaitForSeconds (0.001f);
			yield break;
		}
	}
	IEnumerator  ShotM(Transform shotPositionM){
		if (spaceship.canShot == false) {
			yield break;
		}
		
		while (true) {
			
			// 子要素を全て取得する
			//	for (int i = 0; i < transform.childCount; i++) {
			//	shotPosition = transform.GetChild (i);
			spaceship.ShotMawaru (shotPositionM);
			//	}
			// shotDelay秒待つ
			yield return new WaitForSeconds (10.0f);
			yield break;
		}
	}
	IEnumerator  ShotRocket(Transform shotPositionRo){
		if (spaceship.canShot == false) {
			yield break;
		}
		
		while (true) {
			
			// 子要素を全て取得する
			//	for (int i = 0; i < transform.childCount; i++) {
			//	shotPosition = transform.GetChild (i);
			spaceship.ShotRocket (shotPositionRo);
			//	}
			// shotDelay秒待つ
			yield return new WaitForSeconds (10.0f);
			yield break;
		}
	}
	IEnumerator  ShotSideL(Transform shotPositionSL){
		if (spaceship.canShot == false) {
			yield break;
		}
		
		while (true) {
			
			// 子要素を全て取得する
			//	for (int i = 0; i < transform.childCount; i++) {
			//	shotPosition = transform.GetChild (i);
			spaceship.ShotSideL (shotPositionSL);
			//	}
			// shotDelay秒待つ
			yield return new WaitForSeconds (10.0f);
			yield break;
		}
	}
	IEnumerator  ShotSideR(Transform shotPositionSR){
		if (spaceship.canShot == false) {
			yield break;
		}
		
		while (true) {
			
			// 子要素を全て取得する
			//	for (int i = 0; i < transform.childCount; i++) {
			//	shotPosition = transform.GetChild (i);
			spaceship.ShotSideR (shotPositionSR);
			//	}
			// shotDelay秒待つ
			yield return new WaitForSeconds (10.0f);
			yield break;
		}
	}
	void Railgun()
	{
		anim.Play("BigBossMainRailgun");

						Transform shotPositionR1 = transform.FindChild ("Railgun_1");
						StartCoroutine (ShotR (shotPositionR1));
						/*Transform shotPositionR2 = transform.FindChild ("Railgun_2");
						StartCoroutine (ShotR (shotPositionR2));
						Transform shotPositionR3 = transform.FindChild ("Railgun_3");
						StartCoroutine (ShotR (shotPositionR3));
						Transform shotPositionR4 = transform.FindChild ("Railgun_4");
						StartCoroutine (ShotR (shotPositionR4));
						Transform shotPositionR5 = transform.FindChild ("Railgun_5");
						StartCoroutine (ShotR (shotPositionR5));*/
		if(animL && animR)
		{
			animL.Play("BigBossLeftRailgun");
			animR.Play("BigBossRightRailgun");
		}
		//animL.SetBool ("Railgun", true);
		//animR.SetBool ("Railgun", true);
	}
	void Mawaru()
	{
		Transform shotPositionM3=transform.FindChild("Mawaru_3");
		StartCoroutine (ShotM (shotPositionM3));
		Transform shotPositionM4=transform.FindChild("Mawaru_4");
		StartCoroutine (ShotM (shotPositionM4));
		MawaruCD = 5.0f;
	}
	void Rocket()
	{
		Transform shotPositionRo1=transform.FindChild("Rocket_1");
		StartCoroutine (ShotRocket (shotPositionRo1));
		Transform shotPositionRo2=transform.FindChild("Rocket_2");
		StartCoroutine (ShotRocket (shotPositionRo2));
		Transform shotPositionRo3=transform.FindChild("Rocket_3");
		StartCoroutine (ShotRocket (shotPositionRo3));
		Transform shotPositionRo4=transform.FindChild("Rocket_4");
		StartCoroutine (ShotRocket (shotPositionRo4));
		RocketCD = 2.0f;
		
		rocketOpen = false;
	}
	void SideL()
	{
		Transform shotPositionSL=transform.FindChild("Side_L");
		StartCoroutine (ShotSideL (shotPositionSL));
		SideCD = 5.0f;
		//Debug.Log("llllllllllll");
	}
	void SideR()
	{
		Transform shotPositionSR=transform.FindChild("Side_R");
		StartCoroutine (ShotSideR (shotPositionSR));
		//Debug.Log("jiiiiiiiiiij");
		
	}
	
	
	
	
	
}