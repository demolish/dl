﻿using UnityEngine;
using System.Collections;

public class BossShip : MonoBehaviour {
	public float speed;
	
	// 弾を撃つ間隔
	public float shotDelay;
	
	// 弾のPrefab
	public GameObject bulletCircular;
	public GameObject bulletRed;
	public GameObject bulletMawaru;
	public GameObject bulletRocket;
	public GameObject bulletSideL;
	public GameObject bulletSideR;

	// 弾を撃つかどうか
	public bool canShot;
	
	// 爆発のPrefab
	public GameObject explosion;
	
	//	体力
	public int life;
	
	// アニメーターコンポーネント
	private Animator animator;
	//rocketshoter
	private GameObject cloneShoter;
	
	void Start ()
	{
		// アニメーターコンポーネントを取得
		animator = GetComponent<Animator> ();
	}
	
	
	//	ダメージ・体力判定
	public bool LifeCheck( int damege )
	{
		//	ダメージ
		life -= damege;
		//	判定
		if (life <= 0)return false;
		else return true;
	}
	
	// 爆発の作成
	public void Explosion ()
	{
		Instantiate (explosion, transform.position, transform.rotation);
	}
	
	// 弾の作成
	public void Shot (Transform origin)
	{
		Instantiate (bulletCircular, origin.position, origin.rotation);
	}
	public void ShotRed (Transform origin)
	{
		Instantiate (bulletRed, origin.position, origin.rotation);
	}
	public void ShotMawaru (Transform origin)
	{
		Instantiate (bulletMawaru, origin.position, origin.rotation);
	}
	public void ShotRocket (Transform origin)
	{
		cloneShoter = (GameObject)Instantiate (bulletRocket, origin.position, origin.rotation);
		cloneShoter.transform.parent = this.transform;
	}
	public void ShotSideR (Transform origin)
	{
		Instantiate (bulletSideR, origin.position, origin.rotation);
	}
	public void ShotSideL (Transform origin)
	{
		Instantiate (bulletSideL, origin.position, origin.rotation);
	}
	// アニメーターコンポーネントの取得
	public Animator GetAnimator()
	{
		return animator;
	}
	
	
}