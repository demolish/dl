﻿using UnityEngine;
using System.Collections;

public class Emitter : MonoBehaviour
{
	// Waveプレハブを格納する
	public GameObject[] waves;
	
	// 現在のWave
	private int currentWave;

	static public string stageName = null;

	static public bool stageClear = false;

	IEnumerator Start ()
	{
		// Waveが存在しなければコルーチンを終了する
		if (waves.Length == 0) {
			yield break;
		}

		while (true) {
			
			stageName = this.transform.parent.name;

			// Waveを作成する
			GameObject g = (GameObject)Instantiate (waves [currentWave], transform.position, Quaternion.identity);
			
			// WaveをEmitterの子要素にする
			g.transform.parent = transform;
			
			// Waveの子要素のEnemyが全て削除されるまで待機する
			while (g.transform.childCount != 0) {
				yield return new WaitForEndOfFrame ();
			}
			
			// Waveの削除
			if (g) 
			{
				Destroy (g);
			}

			// 格納されているWaveを全て実行したらcurrentWaveを0にする（最初から -> ループ）
			if (waves.Length <= ++currentWave) {
				stageClear = true;
				currentWave = 0;
			}
			
		}

	}
}