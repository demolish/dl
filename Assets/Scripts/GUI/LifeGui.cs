﻿using UnityEngine;
using System.Collections;

public class LifeGui : MonoBehaviour {
	
	// 体力
	private int m_life;
	//	元の全体の体力：gauge調整用
	private float origin_life;
	//
	private float origin_gauge_width;
	//	gaugeの減らし具合
	private float raton_displacement;

	public GameObject LifeGauge;

	private Player player;
	private Jack jack;

	// Use this for initialization
	void Start () 
	{
		origin_gauge_width = LifeGauge.transform.localScale.x;

		if( SceneManager.m_create_char == 1)
		{		
			player = GameObject.Find("Marika(Clone)").GetComponent<Player>();

			if(player)
			{
				InitSetLife(player.life);
			}else m_life = -1;
		}
		else if( SceneManager.m_create_char == 2)
		{
			jack = GameObject.Find("Jack(Clone)").GetComponent<Jack>();

			Debug.Log("Choose jack");

			if(jack)
			{
				Debug.Log("find Jack");
				InitSetLife(jack.life);
			}else m_life = -1;
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		if( SceneManager.m_create_char == 1)
		{
			SetLife(player.life);
		}
		else if( SceneManager.m_create_char == 2)
		{
			SetLife(jack.life);
		}

		if( origin_gauge_width * raton_displacement < LifeGauge.transform.localScale.x )
		{
			Vector3 temp_vec = new Vector3 (LifeGauge.transform.localScale.x - 0.01f,
		                     	          LifeGauge.transform.localScale.y,
		                     	          LifeGauge.transform.localScale.z);
			LifeGauge.transform.localScale = temp_vec;
		}

		if( LifeGauge.transform.localScale.x < 0 )
		{
			Vector3 temp_vec = new Vector3 (.0f,
			                        LifeGauge.transform.localScale.y,
			                        LifeGauge.transform.localScale.z);
			LifeGauge.transform.localScale = temp_vec;
		}

	}

	//	体力設定
	public void SetLife(int _life)
	{
		m_life = _life;
		raton_displacement = ((float)m_life) / origin_life;

	}

	//	初期体力取得(プレイヤーで呼ぶ)
	public void InitSetLife(int _life)
	{
		m_life = _life;
		origin_life = (float)m_life;
		raton_displacement = ((float)m_life) / origin_life;
	}

}
