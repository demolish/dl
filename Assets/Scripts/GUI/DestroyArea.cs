﻿using UnityEngine;

public class DestroyArea : MonoBehaviour
{
	void OnTriggerExit2D (Collider2D c)
	{
		// レイヤー名を取得
		string layerName = LayerMask.LayerToName (c.gameObject.layer);
		
		// レイヤー名がBullet (Player)の時は何も行わない
		if (layerName == "Bullet (Player)") return;
		Destroy (c.gameObject);
	}
}