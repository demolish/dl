﻿using UnityEngine;
using System.Collections;

public class ScrollText : MonoBehaviour {

	public float speed = 1.0f;

	public float limitY = 0.0f;
		

	// Update is called once per frame
	void Update ()
	{
		if(this.transform.localPosition.y < limitY)
		{
			this.transform.position += new Vector3( 0,speed * Time.deltaTime,0);
		}
	}
}
