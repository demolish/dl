﻿using UnityEngine;
using System.Collections;

public class JackNotDamColAre : MonoBehaviour {

	//	パンチの種類で敵被弾Effect変える
	public GameObject not_damaged_effect;

	public AudioClip NotDamaged1;
	public AudioClip NotDamaged2;
	//
	private AudioSource audioSource;
	
	void Start()
	{
		//
		audioSource = gameObject.GetComponent<AudioSource>();
	}
	
	public void SetPosition( Vector3 pos )
	{
		transform.position = pos;
	}
	
	void OnTriggerEnter2D (Collider2D c)
	{
		// レイヤー名を取得
		string layerName = LayerMask.LayerToName(c.gameObject.layer);
		
		// レイヤー名がBullet (Enemy)の時は弾を削除
		if( layerName == "Bullet (Enemy)" || layerName == "Enemy")
		{
			int num = Random.Range(0,2);
			if(num == 0)audioSource.PlayOneShot( NotDamaged1 );
			else audioSource.PlayOneShot( NotDamaged2 );
			
			Vector3 angle_enemy = c.gameObject.transform.position - transform.position;
			angle_enemy.Normalize();
			
			Instantiate( not_damaged_effect, transform.position + angle_enemy * 0.5f, transform.rotation );
		}
	}

}
