﻿using UnityEngine;
using System.Collections;

public class Punch : MonoBehaviour {

	//	攻撃力
	public int power;

	//	パンチの種類で敵被弾Effect変える
	private bool normal_punch;

	public void SetPosition( Vector3 pos )
	{
		transform.position = pos;
	}


	public void SetPunchType( bool type )
	{
		normal_punch = type;
	}


	public bool CheckNormalPunch(){
		return normal_punch;
	}


}
