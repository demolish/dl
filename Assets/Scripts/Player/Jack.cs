﻿using UnityEngine;
using System.Collections;

public class Jack : MonoBehaviour {

	//	体力
	public int life;
	//	移動速度制限用
	private float delay_speed = 1.0f;

	// Spaceshipコンポーネント
	private Spaceship spaceship;

	//	アニメーション	
	protected Animator animator;
	//	受け取りオブジェクト
	public GameObject punch_col_area;
	//	内部使用オブジェクト
	GameObject  punch_obj;
	//	パンチあたり判定の中心座標
	private Vector3 default_poa = new Vector3( .0f, 0.7f, .0f );
	private Punch punch;


	//	無ダメージ被弾エフェクト用
	public GameObject not_dam_area;
	private JackNotDamColAre not_dam_col;

	//
	public AudioClip Damaged;
	public AudioClip Wind;
	//
	private AudioSource audioSource;


	//	連続パンチチャージ
	private int punch_charge = 0;
	//	連続パンチのチャージ時間
	public int PUNCH_CONTINUTY_CHARGE_TIME;
	//	連続パンチのチャージ時間
	public float PUNCH_CONTINUTY_MOVE_SPEED;
	//	左右パンチどちらから始めるかフラグ
	//	攻撃終了時に左右変更
	//	true:左　false:右
	private bool punch_side = true;
	//	現在のアニメーションクリップ名取得
	private AnimatorStateInfo stateInfo;
	
	//	被弾時のエフェクト
	public GameObject damaged_effect;
	
	//Background 
	Background background;

	private int punch_col_right = 0;
	private int punch_col_left = 0;

	void Start()
	{
		//
		audioSource = gameObject.GetComponent<AudioSource>();

		// Spaceshipコンポーネントを取得
		spaceship = GetComponent<Spaceship>();
		//
		animator = GetComponent<Animator>();
		//	パンチ当たり判定用設定
		punch_obj = Instantiate (punch_col_area, transform.position + default_poa, transform.rotation) as GameObject ;
		punch = punch_obj.GetComponent<Punch> ();
		punch_obj.SetActive(false);

		//	無ダメージ判定用設定
		GameObject  not_dam_obj = Instantiate (not_dam_area, transform.position, transform.rotation) as GameObject ;
		not_dam_col = not_dam_obj.GetComponent<JackNotDamColAre> ();

		//	体力設定
		//life = 100000;

		//	左パンチスタート
		animator.SetBool("punch_left_flag", true );
		animator.SetBool("punch_right_flag", false );

		background = FindObjectOfType<Background>();
	}
	
	void Update ()
	{
		// 右・左
		float x = Input.GetAxisRaw ("Horizontal");
		
		// 上・下
		float y = Input.GetAxisRaw ("Vertical");
		
		// 移動する向きを求める
		Vector2 direction = new Vector2 (x, y).normalized;

		Punch ();

		// 移動の制限
		//Move (direction);
		MoveInBackGround(direction);

		//	無ダメージ座標更新
		not_dam_col.SetPosition (transform.position);
	}

	void MoveInBackGround(Vector3 direction)
	{
		Vector2 scale = background.transform.localScale;
		
		Vector2 min = scale * -0.5f;
		
		Vector2 max = scale * 0.5f;
		
		Vector3 pos = transform.position;
		
		pos += direction * spaceship.speed * Time.deltaTime * delay_speed;
		
		pos.x = Mathf.Clamp( pos.x, min.x, max.x);
		pos.y = Mathf.Clamp( pos.y, min.y, max.y);
		
		transform.position = pos;		
	}
	
	//	ダメージ・体力判定
	private bool LifeCheck( int damege ){
		//	ダメージ
		life -= damege;
		//	表示用に体力調整
		if (life < 0)life = 0;
		//	削除判定
		if (life <= 0)return false;
		else return true;
	}
	
	// ぶつかった瞬間に呼び出される
	void OnTriggerEnter2D (Collider2D c){
		// レイヤー名を取得
		string layerName = LayerMask.LayerToName(c.gameObject.layer);
		
		// レイヤー名がBullet (Enemy)またはEnemyの場合は爆発
		if( layerName == "Bullet (Enemy)" || layerName == "Enemy")
		{
			if( !LifeCheck( 4 ) ) 
			{
				// 爆発する
				spaceship.Explosion ();

				// プレイヤーを削除
				Destroy (gameObject);
				
				// Managerコンポーネントをシーン内から探して取得し、GameOverメソッドを呼び出す
				FindObjectOfType<SceneManager>().GameOver();
			}
			else
			{
				//
				audioSource.PlayOneShot( Damaged );
				
				Vector3 angle_enemy = c.gameObject.transform.position - transform.position;
				angle_enemy.Normalize();
				Instantiate( damaged_effect, transform.position + angle_enemy * 0.1f, transform.rotation );
			}
		}
		
		// レイヤー名がBullet (Enemy)の時は弾を削除
		if( layerName == "Bullet (Enemy)")
		{
			// 弾の削除
			Destroy(c.gameObject);
		}
	}

	public void InstantiatePlayer()
	{
		Instantiate (this, this.transform.position, this.transform.rotation);
	}

	//	連続パンチの音をある程度の間隔で鳴らすため
	private int sound_checker;
	//	パンチ管理
	//	パンチ管理
	void Punch()
	{
		stateInfo = animator.GetCurrentAnimatorStateInfo(0);		
		if( stateInfo.IsName("Base Layer.jack_wait") )
		{
			animator.SetBool("punch_flag", false );
			punch_obj.SetActive(false);
		}
		
		//	あたり判定
		punch.SetPosition( transform.position + default_poa );
		
		//	押しっぱなしで連続パンチチャージ
		if( Input.GetKey (KeyCode.Z) )punch_charge++;
		
		//	チャージタイム経過後連続パンチ
		if ( punch_charge >= PUNCH_CONTINUTY_CHARGE_TIME )
		{
			//	更新
			sound_checker++;
			if( sound_checker % 10 == 0){
				audioSource.PlayOneShot( Wind );
			} 

			punch.SetPunchType(false);
			//	移動制限
			delay_speed = 0.5f;
			
			animator.SetBool("continuty_punch_flag", true );
			animator.SetBool("punch_flag", true );
			
			if( stateInfo.IsName("Base Layer.left_punch") )
			{
				punch_col_left++;
				punch_col_right = 0;


				if( punch_col_left > 5 )
				{
					punch_obj.SetActive(true);
				}
				else punch_obj.SetActive(false);
			}
			if( stateInfo.IsName("Base Layer.right_punch") )
			{
				punch_col_right++;
				punch_col_left = 0;

				if( punch_col_right > 5 )
				{
					punch_obj.SetActive(true);
				}
			}
		}
		else {
			delay_speed = 1.0f;
			sound_checker = 0;
		}
		
		//	キー離した
		if (Input.GetKeyUp (KeyCode.Z) )
		{
			//　通常攻撃
			if ( punch_charge < 30 )
			{
				audioSource.PlayOneShot( Wind );
				animator.SetBool("punch_flag", true );
				punch_obj.SetActive(true);
				Debug.Log("通常パンチ");

				punch.SetPunchType(true);
			}
			
			//	パンチ終わり
			animator.SetBool("continuty_punch_flag", false );
			
			punch_side = !punch_side;
			//	次に左パンチスタート
			if( punch_side )
			{
				animator.SetBool("punch_left_flag", true );
				animator.SetBool("punch_right_flag", false );
			}
			else //　次に右パンチスタート
			{
				animator.SetBool("punch_left_flag", false );
				animator.SetBool("punch_right_flag", true );
			}
			punch_charge = 0;
		}
	}
}