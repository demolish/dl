﻿using UnityEngine;
using System.Collections;

public class Chain : MonoBehaviour {
	
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void CheckDestroy( Vector3 parent_trans )
	{
		if( (parent_trans - transform.position).magnitude < 0.25f )Destroy (gameObject);
		if( parent_trans.y >= transform.position.y )Destroy (gameObject);
	}
}
