﻿using UnityEngine;
using System.Collections;


public class Player : MonoBehaviour
{
	
	///
	///
	/// 指定のオブジェクトの方向に回転する
	/// 
	/// 
	public Quaternion func_LookAt2D( Transform self, Vector3 target, Vector2 forward)
	{
		var forwardDiff = GetForwardDiffPoint (forward);
		Vector3 direction = target - self.position;
		float angle = Mathf.Atan2(direction.y,direction.x) * Mathf.Rad2Deg;
		self.rotation = Quaternion.AngleAxis(angle - forwardDiff, Vector3.forward);
		return self.rotation;
	}
	private float GetForwardDiffPoint(Vector2 forward)
	{
		if(Equals (forward, Vector2.up)) return 90;
		if(Equals (forward, Vector2.right)) return 0;
		return 0;
	}

	private int rolling_charge_time = 0;
	private bool charge_flag = false;
	
	//	体力
	public int life;
	
	// Spaceshipコンポーネント
	private Spaceship spaceship;
	
	//
	private bool shot_flag = false;
	private bool rolling_flag = false;
	private bool finish_rolling = true;
	
	private Vector3 hummer_default_pos = new Vector3 (.2f, .2f, .0f);
	
	public GameObject hummmer_object;
	private Hummer hummer;

	protected Animator animator;

	//	被弾時のエフェクト
	public GameObject damaged_effect;
	//	無ダメージ被弾エフェクト用
	public GameObject not_dam_area;
	private PlayerNotDamColAre not_dam_col;

	//
	public AudioClip Damaged;
	public AudioClip NomHammer;
	public AudioClip RolHammer;
	//
	private AudioSource audioSource;

	//Background 
	Background background;
	
	//	必殺技でのボスとの距離判定
	public float deadly_boss_length;

	private int shot_charge = 0;
	
	void Start()
	{
		//
		audioSource = gameObject.GetComponent<AudioSource>();

		// Spaceshipコンポーネントを取得
		spaceship = GetComponent<Spaceship>();

		animator = GetComponent<Animator>();

		background = FindObjectOfType<Background>();
		
		GameObject temp_hummmer_object = Instantiate (hummmer_object, transform.position + hummer_default_pos, transform.rotation)as GameObject;
		hummer = temp_hummmer_object.GetComponent<Hummer>();

		//	無ダメージ判定用設定
		GameObject  not_dam_obj = Instantiate (not_dam_area, transform.position, transform.rotation) as GameObject ;
		not_dam_col = not_dam_obj.GetComponent<PlayerNotDamColAre> ();
		
		//	体力設定
		//life = 10000;
	}
	
	void Update ()
	{
		//	判定対象との距離によって色を変える
		GameObject boss_enemy = GameObject.FindWithTag("BossEnemy");
		//GameObject[] boss_enemy = GameObject.FindGameObjectsWithTag("BossEnemy");
		if(boss_enemy)hummer.ChangeScale( boss_enemy.transform.position );
		else if(hummer)hummer.ResetScale();
		
		// 右・左
		float x = Input.GetAxisRaw ("Horizontal");
		
		// 上・下
		float y = Input.GetAxisRaw ("Vertical");
		
		// 移動する向きを求める
		Vector2 direction = new Vector2 (x, y).normalized;
		
		// 移動の制限
		//Move (direction);
		MoveInBackGround(direction);

		//	無ダメージ座標更新
		not_dam_col.SetPosition (transform.position);

		hummer.SetPlayerPos( transform.position );
		
		if( Input.GetKey(KeyCode.Z) && !shot_flag && finish_rolling )
		{
			//	攻撃の初期動作
			rolling_charge_time++;
			if( rolling_charge_time > 30 )
			{
				rolling_flag = true;
				hummer.Rolling();

				audioSource.clip = RolHammer;
				if(!audioSource.isPlaying)
				{
					audioSource.Play();
				}
			}
		}
		else if( rolling_flag )
		{
			finish_rolling = false;

			//	途中で通常攻撃が押されたら
			if( Input.GetKeyDown(KeyCode.Z) )
			{
				hummer.SetTransform( transform.position + hummer_default_pos, 
				                    func_LookAt2D( hummer.transform, new Vector3( .0f,100.0f,.0f ), new Vector2( .0f , 1.0f ) ) );
				shot_flag = true;
				rolling_charge_time = 0;
				rolling_flag = false;
				finish_rolling = true;
			}
			//	回転攻撃が終わったら
			if( hummer.EndRolling() )
			{
				rolling_charge_time = 0;
				rolling_flag = false;
				finish_rolling = true;
			}
		}
		
		//	通常攻撃
		if( Input.GetKeyUp(KeyCode.Z) && !rolling_flag && !shot_flag )
		{
			charge_flag = true;
			animator.SetBool("Shot", true );
			rolling_charge_time = 0;

			audioSource.PlayOneShot( NomHammer );
		}

		//	アニメーションに合わせるためにショットを数フレーム遅延
		if( charge_flag == true){
			shot_charge++;
		}
		else { shot_charge = 0; }
		if (shot_charge > 10)shot_flag = true;
		if( shot_flag )hummer.Shot();
	}

	void MoveInBackGround(Vector3 direction)
	{
		Vector2 scale = background.transform.localScale;

		Vector2 min = scale * -0.5f;
	
		Vector2 max = scale * 0.5f;

		Vector3 pos = transform.position;

		pos += direction * spaceship.speed * Time.deltaTime;

		pos.x = Mathf.Clamp( pos.x, min.x, max.x);
		pos.y = Mathf.Clamp( pos.y, min.y, max.y);

		transform.position = pos;

		if( shot_flag == false && rolling_flag == false)
		{
			pos.x += hummer_default_pos.x;
			pos.y += hummer_default_pos.y;
			hummer.SetAccelerator( new Vector3(pos.x , pos.y ,0 ) );
			hummer.transform.rotation = func_LookAt2D( hummer.transform, new Vector3( .0f,100.0f,.0f ), new Vector2( .0f , 1.0f ) );
		}
	}
	
	//	ダメージ・体力判定
	private bool LifeCheck( int damege ){
		//	ダメージ
		life -= damege;
		//	表示用に体力調整
		if (life < 0)life = 0;
		//	削除判定
		if (life <= 0)return false;
		else return true;
	}
	
	// ぶつかった瞬間に呼び出される
	void OnTriggerEnter2D (Collider2D c){
		// レイヤー名を取得
		string layerName = LayerMask.LayerToName(c.gameObject.layer);
		
		// レイヤー名がBullet (Enemy)またはEnemyの場合は爆発
		if( layerName == "Bullet (Enemy)" || layerName == "Enemy")
		{
			if( !LifeCheck( 4 ) ) 
			{
				// 爆発する
				spaceship.Explosion ();

				// プレイヤーを削除
				Destroy (gameObject);
					
				// Managerコンポーネントをシーン内から探して取得し、GameOverメソッドを呼び出す
				FindObjectOfType<SceneManager>().GameOver();
			}
			else
			{
				//
				audioSource.PlayOneShot( Damaged );

				Vector3 angle_enemy = c.gameObject.transform.position - transform.position;
				angle_enemy.Normalize();
				Instantiate( damaged_effect, transform.position + angle_enemy * 0.1f, transform.rotation );
			}
		}
		
		// レイヤー名がBullet (Enemy)の時は弾を削除
		if( layerName == "Bullet (Enemy)")
		{
			// 弾の削除
			Destroy(c.gameObject);
		}
	}
	
	public void SetShotFlag()	{
		shot_flag = false;
		charge_flag = false;
		animator.SetBool("Shot", false );
		shot_charge = 0;
	}
	
	public void SetRollingFlag()	{
		rolling_flag = false;
	}
	
	public void InstantiatePlayer()
	{
		Instantiate (this, this.transform.position, this.transform.rotation);
	}
	
}