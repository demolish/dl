﻿using UnityEngine;
using System.Collections;

public class Hummer : MonoBehaviour 
{
	// 弾の移動スピード
	public int speed;
	//	ローリングスピード
	public float rolling_speed;
	
	//
	private int ration = 0;

	//	チェーンの距離
	public int chain_length;
	
	//表示ﾁｪｰﾝの数
	private int chain_num_counter = 0;
	
	//
	private Transform temp_toransform;
	
	private Vector3 origin_scale;
	
	//	必殺技でのボスとの距離判定
	public float deadly_boss_length; 
	
	// 攻撃力
	public int power;
	public int power_nom;
	public int power_rol;
	
	public GameObject chain;
	
	private Vector3 player_pos;

	//	ノーマルショットかどうか(相手の被弾Effectの判定用)
	private bool normal_shot =true;

	void Start (){
		origin_scale = this.transform.localScale;
		GetComponent<TrailRenderer>().enabled = false; 

		power = power_nom;
	}
	
	public void SetPlayerPos (Vector3 pos){
		player_pos = pos;
	}
	
	public void SetAccelerator( Vector3 move){
		transform.position = move;
	}
	
	public void Shot(){
		power = power_nom;
		//	通常ショット
		normal_shot = true;
		//	トレイルEffect使用
		GetComponent<TrailRenderer> ().enabled = true; 
		//	座標設定
		transform.position = new Vector3(player_pos.x + 0.2f, transform.position.y, transform.position.z);
		
		ration++;
		Vector2 velocity = new Vector2 (0, 0.2f);

		//	ハンマーとの距離
		float length_to_player = ( player_pos - transform.position ).magnitude;
		
		//	チェーン管理
		int ChildCount1 = transform.childCount;
		//	最後に生成されたチェーンの座標を取得(チェーンとの距離を算出するため)
		Vector3 last_pos;
		if( ChildCount1 == 0 ){
			last_pos = transform.position;
		}
		else 
		{
			GameObject child_obj = transform.GetChild(ChildCount1-1).gameObject;
			last_pos = child_obj.transform.position;
		}
		//	最後に生成したチェーンとの距離
		float chiled_length_to_player = ( player_pos - last_pos ).magnitude;
		
		//	最大延長距離
		if( ration < chain_length )
		{
			rigidbody2D.velocity = velocity * speed;

			//	１チェイン単位(0.3f)で距離が離れたらチェイン数を増やす
			while( chiled_length_to_player > 0.3f )
			{
				//	チェーン座標
				Vector3 chain_position = new Vector3( transform.position.x, 
				                                     last_pos.y - 0.15f, 
				                                     transform.position.z );
				//	チェーン生成
				GameObject child_obj = Instantiate(chain, chain_position, transform.rotation) as GameObject;
				child_obj.transform.parent = transform;

				ChildCount1 = transform.childCount;
				child_obj = transform.GetChild(ChildCount1-1).gameObject;
				last_pos = child_obj.transform.position;
				chiled_length_to_player = ( player_pos - last_pos ).magnitude;
			}
		}
		else 
		{
			if( length_to_player > 0.35f )
			{
				rigidbody2D.velocity = velocity * -speed;
				int ChildCount = transform.childCount;
				for(int i=0; i<ChildCount; i++)
				{
					GameObject child_obj = transform.GetChild(i).gameObject;
					Chain chain_commp = child_obj.GetComponent<Chain>();
					chain_commp.CheckDestroy(player_pos);
				}
			}else{
				int ChildCount2 = transform.childCount;
				for(int i=0; i<ChildCount2; i++)
				{
					GameObject child_obj = transform.GetChild(i).gameObject;
					Destroy(child_obj);
				}
				ration = 0;
				chain_num_counter = 0;
				FindObjectOfType<Player>().SetShotFlag();
				GetComponent<TrailRenderer> ().enabled = false; 
			}
		}
	}
	
	public void Destroy(){
		Destroy(gameObject);
	}
	
	public void ChangeScale( Vector3 enemy_boss_position){
		float length_to_boss = (enemy_boss_position - transform.position).magnitude;
		if( length_to_boss > deadly_boss_length)length_to_boss = deadly_boss_length;
		float change_scale_ration =  2.0f - ( length_to_boss / deadly_boss_length );
		this.transform.localScale = origin_scale * change_scale_ration;
	}
	
	public void ResetScale(){
		this.transform.localScale = origin_scale;
	}
	
	public bool Rolling(){
		power = power_rol;
		normal_shot = false;
//		//	エフェクト生成
		GetComponent<TrailRenderer> ().enabled = true; 

		//	ローカルの右方向へ移動
		Vector3 to_player = transform.position - player_pos;
		to_player.Normalize();
		Vector3 right_vec = Vector3.Cross (to_player, new Vector3(0, 0, 1));
		Vector2 right_2d_vec = new Vector2( right_vec.x, right_vec.y);
		rigidbody2D.velocity = right_2d_vec * rolling_speed;
		transform.LookAt (right_2d_vec);
		
		//	プレイヤーの逆方向に向き設定
		to_player = transform.position - player_pos;
		float player_to_length = to_player.sqrMagnitude;
		to_player.Normalize ();
		if(player_to_length > 5.0f)transform.position = player_pos + to_player * 2.0f;	//	最大距離
		float angle = Mathf.Atan2(to_player.y,to_player.x) * Mathf.Rad2Deg;
		transform.rotation = Quaternion.AngleAxis(angle - 90.0f, Vector3.forward);
		
		//	チェーンの実装
		to_player = player_pos - transform.position;
		float length_to_player = to_player.magnitude;
		//	１チェイン単位で距離が離れたら数を増やす
		int temp_chain_num = (int)(length_to_player / 0.1f);
		if( chain_num_counter < temp_chain_num ){
			chain_num_counter = temp_chain_num;
			to_player.Normalize();
			Vector3 chain_position = transform.position + to_player * chain_num_counter * 0.08f;
			GameObject child_obj = Instantiate(chain, chain_position, transform.rotation) as GameObject;
			child_obj.transform.parent = transform;
		}
		return true;
	}
	
	public bool EndRolling()
	{
		//	ローカルの右方向へ移動
		Vector3 to_player = transform.position - player_pos;
		to_player.Normalize();
		Vector3 right_vec = Vector3.Cross (to_player, new Vector3(0, 0, 1));
		Vector2 right_2d_vec = new Vector2( right_vec.x, right_vec.y);
		rigidbody2D.velocity = right_2d_vec * rolling_speed;
		transform.LookAt(right_2d_vec);
		
		//	プレイヤーの逆方向に向き設定
		to_player = transform.position - player_pos;
		transform.position = player_pos + to_player * 0.8f;
		to_player = transform.position - player_pos;
		float angle = Mathf.Atan2(to_player.y,to_player.x) * Mathf.Rad2Deg;
		transform.rotation = Quaternion.AngleAxis(angle - 90.0f, Vector3.forward);

		//	一定距離近づいたらチェーン全削除
		to_player = transform.position - player_pos;
		float player_to_length = to_player.sqrMagnitude;
		if( player_to_length < 0.6f )
		{
			int ChildCount1 = transform.childCount;
			for(int i=0; i<ChildCount1; i++)
			{
				GameObject child_obj = transform.GetChild(i).gameObject;
				Destroy(child_obj);
			}
			chain_num_counter = 0;
			GetComponent<TrailRenderer> ().enabled = false; 
			normal_shot = true;
			power = power_nom;
			return true;
		}

		//	チェーン１つ１つ削除チェック
		int ChildCount = transform.childCount;	
		for(int i=0; i<ChildCount; i++){
			GameObject child_obj = transform.GetChild(i).gameObject;
			Chain chain_comp = child_obj.GetComponent<Chain>();
			chain_comp.CheckDestroy(player_pos);
		}

		return false;
	}
	
	public void SetTransform( Vector3 pos, Quaternion rot )
	{
		transform.position = pos;
		transform.rotation = rot;
		int ChildCount = transform.childCount;		
		for(int i=0; i<ChildCount; i++){
			GameObject child_obj = transform.GetChild(i).gameObject;
			Destroy(child_obj);
		}
	}
		
	void OnTriggerEnter2D(Collider2D c){
		// レイヤー名を取得
		string layerName = LayerMask.LayerToName(c.gameObject.layer);
		if( layerName == "Player"){
			//	フラグ設定
			FindObjectOfType<Player>().SetShotFlag();
			
			// 弾の削除
			ration = 0;
		}		
	}

	public bool CheckNormalShot(){
		return normal_shot;
	}
}