﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
        StartCoroutine("Wait");
	}

    private IEnumerator Wait()
    {
        yield return new WaitForSeconds(1.0f);

        Destroy(this.gameObject);
    }
}
