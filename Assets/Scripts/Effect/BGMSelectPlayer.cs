﻿using UnityEngine;
using System.Collections;

public class BGMSelectPlayer : MonoBehaviour {

	public AudioClip door;

	private AudioSource audioSource;

	// Use this for initialization
	void Start () {
		audioSource = gameObject.GetComponent<AudioSource>();
	}

	public void playAudioDoor()
	{
		audioSource.clip = door;

		audioSource.Play();
	}
}
