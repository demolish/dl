﻿using UnityEngine;

public class Cloud : MonoBehaviour
{
	// スクロールするスピード
	public float speed = 0.01f;

	// Y 
	public float limitY = -12.0f;

	void Update ()
	{
		MoveCloud();
	}

	void MoveCloud()
	{ 
		this.gameObject.transform.position += new  Vector3 (0,-speed,0);

		if(this.gameObject.transform.position.y <= limitY)
		{
			this.gameObject.transform.position = new Vector3 (0,-limitY,0);
		}
	}
}
