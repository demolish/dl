﻿using UnityEngine;
public class FadeTexture : MonoBehaviour {

	private bool m_play;
	private bool m_fadeIn;
	private float time;

	// Use this for initialization
	void Start ()
	{
		m_play = false;
		m_fadeIn = false;
		time = 0;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(m_play)
		{
			time += Time.deltaTime * 0.5f; 

			if(m_fadeIn)
			{
				guiTexture.color = new Color(1,1,1,Mathf.Lerp(1,0,time));
			}else{
				guiTexture.color = new Color(1,1,1,Mathf.Lerp(0,1,time));
			}
		}

		if(time >= 1)
		{
			m_play = false;
		}

	}

	public void setFadeAlphaZero()
	{
		guiTexture.color = new Color(1,1,1,0);
	}

	public void startFadeIn()
	{
		m_play = true;
		m_fadeIn = true;
		time = 0;
	}

	
	public void startFadeOut()
	{
		m_play = true;
		m_fadeIn = false;
		time = 0;
	}
}
