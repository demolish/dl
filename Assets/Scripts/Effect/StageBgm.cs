﻿using UnityEngine;
using System.Collections;

public class StageBgm : MonoBehaviour {

	public AudioClip bgm;

	public void setBGM()
	{
		audio.clip = bgm;
	}

	public void playBackGroundMusic()
	{
		if(audio && !audio.isPlaying)
		{
			audio.Play();
		}
	}

	public void stopBackGroundMusic()
	{
		if(audio && audio.isPlaying)
		{
			audio.Stop();
		}
	}
}
